# footsteps.el


## License

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 3, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNU Emacs; see the file COPYING.  If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.


## Description

This package provides a way to quickly retrace your "footsteps" when navigating a project.
It maintains a ring of automatically-logged previous locations called "footsteps", or "steps", for short.
The steps are logged "intelligently", based on certain settings, which are customizable.
The steps are stored in the step-ring, which can be conceptualized as a trail of footsteps.

Note that footsteps-mode is designed to be active all the time, and the user-facing functions should ideally be mapped to very short key chords.
For this reason this package does not automatically assign any keybindings, as they would likely conflict with many users' existing keybindings.
It is recommended that you assign a short keybinding for step-backward and step-forward, at a minimum.


## Installation

To install footsteps, simply add the directory that contains footsteps.el to the load-path.
Then, in your init file, require footsteps, and enable footsteps-mode.
An example is shown below.
<pre>

(add-to-list 'load-path "~/.emacs.d/footsteps")
(require 'footsteps)
(footsteps-mode)
</pre>


## User-facing Commands
<pre>

The commands which are designed to be user-facing are as follows:

          footsteps-mode:  Toggles the footsteps minor mode in all buffers.
                           When footsteps-mode is enabled, it will log locations/steps in the step-ring.
         clear-step-ring:  Clears all existing steps from the step-ring.
                           Note that it will then log the current location as the first step in the step-ring, using the default settings.
  unlog-step-at-location:  Removes the current location, if stored, from the step-ring.
            step-forward:  Moves forward one step, unless you are at the most recent end of the trail.
           step-backward:  If you have ventured off the trail of footsteps, without leaving any new ones, it will first take you to the last step you were on.
                           Otherwise, it will take you to the previous step, unless you are at the oldest end the trail.
</pre>


## What is a footstep?
<pre>
_________________________________
|       |        |        |      |
| Frame | Window | Buffer | Line |
|_______|________|________|______|

At all times within the emacs interface, the point exists at a particular location.
This location can be defined by its frame, window, buffer, and line number.
A footstep (frequently referred to as a step) is a list of those 4 items, which can be used to restore any location that is still accessible.
</pre>


## What is the step-ring?
<pre>
         _________
         |        |    The step-ring, as the name suggests, is a ring of steps.																		 
         | Step 1 |    It can be thought of like a trail of footsteps, which get left behind as you navigate the interface.							 
         |________|    Just like navigating a trail in the woods, the footsteps you leave behind happen automatically.									 
         |        |    Some steps that you take leave a footstep, and some do not.																		 
         | Step 2 |    This package will use a group of settings to determine how to maintain the step-ring.											 
         |________|    To modify these settings, simply customize them. They can be found in the footsteps group of custom variables.					 
         |        |    Each setting in the footsteps group has a documentation string, so an explanation of each one can be found in the customize menu.
         | Step 3 |    
         |________|    
         |        |    
         | Step 4 |    
         |________|    
         |        |    At all times, there exists a step-index, which is your current position within the step-ring.
    ---> | Step 5 |    In the example to the left, the index is on Step 5.
         |________|    As you step-backward and step-forward, this index is decremented or incremented, to keep track of your current position within the step-ring.
         |        |    
         | Step 6 |    Most of the time, the step-index will reside on the most recent step, which would be Step 8 in this example.
         |________|    This is because, as steps are logged, you are, by definition, on that step.
         |        |    
         | Step 7 |    
         |________|    
         |        |    
         | Step 8 |    
         |________|    
</pre>


## What events trigger the footsteps package to do something?

All user commands trigger a response from this package.
The hook that this package uses is 'post-command-hook'.
This hook triggers the command 'footsteps--maintain-step-ring', which is the entry point for all automatic behavior.
Some tasks are performed after every command, but the most critical one, logging, is not performed every time.
A number of criteria are used to determine whether or not a step should be logged.


## In-transit vs destination locations:

When the user moves from one location to another, every interim location could, theoretically, be a step.
As an example, let's say the user is currently doing work on line 30, in a buffer.
The user then needs to copy some text from line 1000, and paste it onto line 30.
To do so, they begin to scroll with the touchpad or mouse wheel, to line 1000.
When they arrive, they copy the text, and use the command step-backward to take them back to the vicinity of line 30.
In this example, it would be ideal if the footsteps package "realized" that all locations between line 30 and 1000 were in-transit and did not log any of them.
The footsteps package attempts to do this, primarily through the use of the customizable function list 'footsteps--abort-hook-commands'.
If the command which triggered the main event function (described above), is found to be a member of this list, it will not log.
To address the example described above, the default list contains the command 'mwheel-scroll'.
Another customizable list which is used to prevent in-transit logging is 'footsteps--abort-hook-buffers'.
This list contains the names of any buffers, for which logging should never occur.
An example default member is "*Buffer List*", as the buffer list is typically always an in-transit location.
As another example, let's say the user gets back to line 30, and pastes the text, only to realize they copied the wrong thing.
They need to jump back to line 1000, so they run the command step-forward. Using the default settings, a step would have been logged at line 1000, so they would be taken back to it.
The step would have been logged at line 1000 because, to copy the text, the user would have had to run commands that aren't found in 'footsteps--abort-hook-commands'.


## Minimum distance between steps:

To describe the minimum distance between steps, let's use another example.
Let's say a user is on line 30, in a buffer.
Again, they need to copy some text from line 1000.
However, they also need to add some line numbers for lines 190 through 200.
Let's say the user scrolls from line 30 to line 190, and begins adding line numbers manually.
They then continue to line 1000 to copy some text, and attempt to use step-backward to get back to line 30.
The first time they run it, they are taken to the vicinity of the line number edit.
Obviously, it is ideal that the footsteps package would have recognized that the user doesn't want a step on every line, between 190 and 200.
Instead, it should have only logged a single step in this general vicinity.
For this reason, there is a minimum "distance" between a new step and any past "relevant" steps, as well as between 2 adjacent steps in the step-ring.
The minimum distance is achieved when the steps are in different frames, windows or buffers, as well as if they are a minimum of some number of lines apart.
The minimum difference between the line numbers of a new step and past "relevant" steps, as well as adjacent steps in the step-ring, is a customizable setting.
It can be changed by customizing the 'footsteps--min-lines-before-step' setting.
In general, its value should be high enough to prevent multiple steps from being logged at a single perceived location.
It should also be small enough to ensure a step gets logged in the general vicinity of any target location in the user's history.
The default value is designed to be a little less than half of the number of lines visible in a window at any given time, but obviously, this will vary between users.


## Unlogging a step:

In the line number example above, it's explained that, with default settings, the footsteps package would not log a step on every line from 190 to 200.
It would log only a single step in that location.
Let's suppose, however, that the user needs to navigate back and forth between line 30 and 1000.
The user would likely not want to continue stepping across any location at all, near the line edit.
To prevent this, the user could navigate to the undesired step, and run the command 'unlog-step-at-location'.
After running that command, it's important to not do anything that could cause the step to be relogged.
In general, after unlogging a step, the next action should be to step-forward or step-backward, to leave that location without relogging it.


## User navigation patterns:

User navigation can be categorized into 3 distinct types, and each one can provoke different behavior from the footsteps package.
The 3 navigation patterns are linear, cyclic, and branching.  They are described below.


### Linear navigation:
<pre>
 _____________ 
 |            |    A linear step sequence is the most straightforward type of sequence.
 | Location 1 |    It is a step sequence in which all steps are unique.
 |____________|    These are the "best" step sequences, as they fit nicely in the linear step-ring, and don't cause confusing step-ring navigation.
 |            |    
 | Location 2 |    
 |____________|    
 |            |    
 | Location 3 |    
 |____________|    
</pre>


### Cyclic navigation:
<pre>
 _____________
 |            |    A cyclic step sequence (or step cycle) is a step sequence whose start and end locations are the same.                                                                  
 | Location 1 |    An example step-ring with step-cycles is shown to the left.                                                                                                            
 |____________|    Obviously, in this case, it would be ideal if the footsteps package recognized the cycle, and only logged locations 2 and 3 one time each.                             
 |            |    Using the default settings, that's what the package would do.                                                                                                          
 | Location 2 |    Let's consider another example though. Let's assume the user has 2 windows open.                                                                                       
 |____________|    They clear the step-ring while in window 1, making their current location the only step in the ring.                                                                   
 |            |    They then move to the other window, and navigate within it until their step-ring is 50 steps long.                                                                     
 | Location 3 |    They then click back to the initial location in step 1.                                                                                                                
 |____________|    Obviously, in this case, it would be ideal if the footsteps package logged location 1 again, as the user clearly needs to navigate between location 1 and location 50.
 |            |    Having to step backward and forward 49 times would not make for convenient navigation.                                                                                 
 | Location 2 |    Whether or not the footsteps package will log a duplicate of a pre-existing step is determined                                                                         
 |____________|      by comparing the length of the resultant step-cycle to the customizable 'footsteps--shortest-permissible-step-cycle' setting.                                        
 |            |    In the event that a duplicate step is not logged, the step-index will be adjusted to the location of the conflicting step.                   
 | Location 3 |    In other words, let's say you have step sequence 1-2-3, and from 3, you manually navigate back to 2.                                                                 
 |____________|    You would then use the step-forward command to navigate to step 3, rather than step-backward.
 |            |    
 | Location 4 |    
 |____________|
</pre>


### Branching navigation:
<pre>
 _____________
 |            |                      To explain branching navigation, let's consider an example.
 | Location 1 |                      Let's say a user has a step-ring that is 4 steps long.
 |____________|                      The user is at the most recent step, which is step/location 4.
 |            |__________            They then step-backward to step 2.
 | Location 2 |          |           From step 2, they navigate to a new location, location 5.
 |____________|         \|/          The navigation in this case is branched, but the step-ring is linear, and cannot contain a branch.
 |            |    ______'______     Because of this, the options are to discard the 5-6 sequence, discard the 3-4 sequence, or violate step adjacency.
 | Location 3 |    |            |    Some example step sequences that would violate adjacency are 1-2-3-4-5-6 or 1-2-5-6-3-4.
 |____________|    | Location 5 |    These sequences violate step adjacency because steps 4 and 5, as well as 6 and 3 were never visited sequentially, by the user.
 |            |    |____________|    As this package is only useful if its behavior is generally predictable for the user, step adjacency is never violated.
 | Location 4 |    |            |    The option to discard the 5-6 sequence is not an option either.
 |____________|    | Location 6 |    This is because this package is most likely to be used to jump between locations in recent history.
                   |____________|    For this reason, newer steps take precedence over older ones, so steps 3 and 4 would be discarded.
                                     The act of discarding the oldest of 2 step sequences after a branching event is called "pruning".
Ultimately, pruning will always occur if a step, which is not a duplicate, is logged when the step-index is not at the most recent step.
If a previous step is navigated to manually, from the most recent step, the 'footsteps--shortest-permissible-step-cycle' setting inadvertently governs pruning.
This is because a lower value for this setting means that it is more likely to log a duplicate of the previous step, keeping you at the end of the step-ring.
If you've navigated to an earlier step-index in the step-ring, and you want to navigate manually without the risk of pruning, you can use 'footsteps-mode' to toggle the minor mode off.
All of the commands and functionality will continue to work with footsteps-mode disabled but no step-ring maintenance will occur.
Once back in a location on the ring, you can re-enable footsteps-mode.
</pre>


## Step location adjustments:
<pre>
 ___________________________________
 |          *Some Buffer*           |    To explain step location adjustments, let's consider an example.
 |                                  |    Let's say a user has 2 steps on the step-ring, and they are in the same buffer.
 |                                  |    An example of what this may look like is shown to the left.
 | Step 1                           |    Let's say the user inserts 3 lines between steps 1 and 2.
 |                                  |    Obviously, it would be ideal if the footsteps package would shift step 2 down by 3 lines, but leave step 1 alone.
 |                                  |    This is what the footsteps package would do in this scenario.
 |                                  |    Any addition or subtraction of lines in a buffer will affect the steps below it, but not the ones above it.
 |                                  |    The more complicated behavior occurs when the modification happens to the line that the step resides on.
 |                                  |    If the line containing a step is deleted, the behavior depends on the customizable setting, 'footsteps--delete-step-with-line'.
 |    -*some text modification*-    |    By default, this setting is nil, and it is recommended to leave it that way.
 |                                  |    When it is nil, no action is taken on the step.
 |                                  |    When it is t, it deletes the step, but this is likely to result in that location being relogged at the end of the step-ring, causing confusing behavior.
 |                                  |    When the point rests on a line with a step, and a line is added, such as with the enter key, the step will not be adjusted.
 |                                  |    When the point rests on a line with a step, and a line is removed, such as with the backspace key, the step will be moved upward.
 |                                  |    Another way to conceptualize this is as an attempt to drag the step with the point.
 | Step 2                           |    Imagine a rubber band being attached to the step and the point, any time the point is on the same line as a step.
 |                                  |    When the point moves to an adjacent line (in such a way that a line is added or removed), the rubber band stretches, and tries to pull the step.
 |__________________________________|    It is possible to pull a step upwards, but it cannot be pulled downwards.
</pre>


## Step-ring cleaning:

Sometimes, when maintaining the step-ring, steps must be removed.
For example, a step may exist in a buffer that has been killed.
Alternatively, modifications made to the buffer could bring 2 steps within the minimum distance requirements of each other.
For this reason, the step-ring is routinely cleaned.
Any step that resides in a location that no longer exists will be deleted, as will the older of any 2 adjacent steps that are found to be too close to each other.


## A note about popwin-mode and the 'footsteps--same-window-mode' setting:

The footsteps package also provides the customizable setting, 'footsteps--same-window-mode'.
The reason for this setting is primarily for compatibility with popwin-mode, but the behavior that it causes could also be preferred by the user.
What this setting does, is to force step-ring navigation to remain in the current window.
Note that the step-ring will still log steps across multiple windows, but when navigating the history, all steps are displayed in the current window.
The reason this is required for popwin-mode compatibility, is that popwin-mode effectively changes the identity of the windows on the screen.
While I haven't verified, I suspect it's not truly changing the window identities, but rather moving existing windows around, in a way that is unnoticeable to the user.
I suspect that it does this in an effort to work around some built-in behaviors from emacs, which are impractical to change.
Regardless, as the window identities can effectively change, popwin-mode can cause step-ring navigation to behave in unpredictable ways.
When this setting is enabled, footsteps-mode effectively ignores windows entirely, always using the current window.
If popwin-mode is being used, the footsteps package will automatically enable this setting, even though it's disabled by default.


## A note about performance with footsteps-mode:

It's worth noting that I had no issues with performance during the testing of footsteps mode.
However, it's conceivable that it could cause some performance issues, given its use of the 'post-command-hook', as well as the looping nature of the package.
If performance issues are encountered immediately after enabling footsteps-mode, it is likely unsuitable for use with your hardware.
However, if performance issues arise over time when using footsteps-mode, it may be advantageous to customize the setting, 'footsteps--max-num-steps'.
This setting determines the size of the step-ring, which will limit the total number of steps that are stored at any one time.
To find an optimal value, it's recommended to use footsteps-mode until performance issues arise, and then determine the current length of of the step-ring.
Reduce that value by some margin of safety, and make that the value of 'footsteps--max-num-steps'.
