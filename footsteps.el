;;; footsteps.el --- A new way to navigate a project.  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Jacob Davis

;; Author: Jacob Davis <contact via gitlab>
;; Maintainer: Jacob Davis <contact via gitlab>
;; Created: 16 Jan 2022
;; Modified: 16 Jan 2022
;; Version: 0.0.1
;; Package-Requires: ((emacs "26.3"))
;; Keywords: footsteps steps back backward forward navigation jump
;; URL: https://gitlab.com/jdavisclemson/footsteps

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; This package provides a way to quickly retrace your "footsteps" when navigating a project.
;; It maintains a ring of automatically-logged previous locations called "footsteps", or "steps", for short.
;; The steps are logged "intelligently", based on certain settings, which are customizable.
;; The steps are stored in the step-ring, which can be conceptualized as a trail of footsteps.
;;
;; Please see README.md from the footsteps repository for more details.

;;; Code:

(defcustom footsteps--max-num-steps 100
  "The maximum number of steps to retain in the step-ring."
  :group 'footsteps
  :type 'integer)

(defcustom footsteps--shortest-permissible-step-cycle 4
  "The shortest possible sequence of steps 
with the same begin and end location.
(must be greater than 1)"
  :group 'footsteps
  :type 'integer)

(defcustom footsteps--min-lines-before-step 20
  "The minimum difference in line number between locations/steps in the same 
frame, window and buffer, before they are considered to be the same location/step.
(must be greater than 0)"
  :group 'footsteps
  :type 'integer)

(defcustom footsteps--abort-hook-commands (list 'left-char
                                                'right-char
                                                'next-line
                                                'previous-line
                                                'mwheel-scroll
                                                'step-backward
                                                'step-forward
                                                'newline
                                                'save-buffer
                                                'save-buffers-kill-terminal
                                                'unlog-step-at-location
                                                'other-window
                                                'windmove-left
                                                'windmove-right
                                                'windmove-up
                                                'windmove-down
                                                'split-window-horizontally
                                                'split-window-vertically
                                                'split-window-below
                                                'split-window-right
                                                'isearch-repeat-forward
                                                'isearch-repeat-backward
                                                'isearch-query-replace-regexp
                                                'isearch-mode
                                                'isearch-yank-string)
  "A list of commands which will not cause the logging of a new step."
  :group 'footsteps
  :type '(function))

(defcustom footsteps--abort-hook-buffers (list "*Buffer List*"
                                               "*Help*"
                                               "*Messages*")
  "A list of buffers in which to never log a step."
  :group 'footsteps
  :type '(string))

(defcustom footsteps--same-window-mode nil
  "Force step-ring navigation to remain in the current window.
Note that this must be t when using popwin-mode."
  :group 'footsteps
  :type 'boolean)

(defcustom footsteps--delete-step-with-line nil
  "If non-nil, the deletion of a line that contains a step
will also delete the step. It is recommended to leave this setting nil, 
as the feature that this enables can cause confusing behavior.
This is because subsequent user actions may cause the step to be logged 
again, at the end of the step-ring, confusing the order of the steps."
  :group 'footsteps
  :type 'boolean)

(define-minor-mode footsteps-mode
  "Toggle footsteps-mode in all buffers."
  :init-value nil
  :global t
  :group 'footsteps
  :lighter " FS"
  :keymap nil
  (if footsteps-mode
      (footsteps--toggle-on)
    (footsteps--toggle-off)))

(defun footsteps--correct-values ()
  "Create or fix global variables that need it."
  (interactive)
  ;; Correct custom variables if needed.
  (when (< footsteps--shortest-permissible-step-cycle 2) (customize-set-value footsteps--shortest-permissible-step-cycle 2))
  (when (< footsteps--min-lines-before-step 1) (customize-set-value footsteps--min-lines-before-step 1))
  (when (and (not footsteps--same-window-mode) (boundp 'popwin-mode) popwin-mode) (customize-set-value footsteps--same-window-mode t))
  ;; Correct non-custom variables if needed.
  (unless (and (boundp 'step-ring) (ring-p step-ring) step-ring)
    (setq-default step-ring (make-ring footsteps--max-num-steps)))
  (unless (and (boundp 'step-index) (integerp step-index))
    (setq-default step-index (max 0 (1- (ring-length step-ring))))
  (unless (and (boundp 'footsteps--buffer-length-ring) (ring-p footsteps--buffer-length-ring) footsteps--buffer-length-ring)
    (progn
      (setq-default footsteps--buffer-length-ring (make-ring 0))
      (ring-insert+extend footsteps--buffer-length-ring (list (current-buffer) (footsteps--buffer-num-lines)) t)))))

(defun footsteps--toggle-on ()
  "Toggle footsteps-mode on."
  (interactive)
  (footsteps--correct-values)
  (add-hook 'post-command-hook 'footsteps--maintain-step-ring)
  (message "Footsteps mode enabled."))

(defun footsteps--toggle-off ()
  "Toggle footsteps-mode off."
  (interactive)
  (remove-hook 'post-command-hook 'footsteps--maintain-step-ring)
  (message "Footsteps mode disabled."))

(defun clear-step-ring ()
  "Removes all steps from the step-ring."
  (interactive)
  ;; Generate a new, empty step-ring.
  (setq-default step-ring (make-ring footsteps--max-num-steps))
  (setq-default footsteps--buffer-length-ring (make-ring 0))
  (ring-insert+extend footsteps--buffer-length-ring (list (current-buffer) (footsteps--buffer-num-lines)) t)
  (message "The step-ring has been cleared."))

(defun footsteps--step-number-ensure-positive (step-number)
  "Returns a positive step-number from the input step-number, 
which can be positive or negative."
  (interactive)
  ;; If the step-number is negative, add it to the total number of steps to get the positive counterpart.
  (when (< step-number 0) (setq step-number (+ (ring-length step-ring) step-number)))
  step-number)

(defun footsteps--get-step (step-number)
  "Returns the step at step-number from the step-ring.
It can take a positive or negative step-number, and returns nil if the step does not exist."
  (interactive)
  (let* (
         ;; Ensure the step-index is in positive format.
         (step-number (footsteps--step-number-ensure-positive step-number))
         ;; Give step a default value of nil, which will be returned if the step doesn't exist (ie, out-of-bounds).
         (step nil)
         )
    ;; As long as we're not out-of-bounds...
    (when (and (>= step-number 0) (< step-number (ring-length step-ring)))
      ;; Get the appropriate step from step-ring.
      (setq step (ring-ref step-ring step-number)))
    step))

(defun footsteps--unlog-step (step-number)
  "Removes the step at step-number from the step-ring.
It can take a positive or negative step-number, and does nothing if the step does not exist."
  (interactive)
  (let* (
         ;; Ensure step-index is in positive format.
         (step-number (footsteps--step-number-ensure-positive step-number))
         )
    ;; If the step exists (ie, we're not out-of-bounds)...
    (when (footsteps--get-step step-number)
      ;; If the step we're deleting is on or above the step we're on, decrement the step-index.
      (when (<= step-number step-index) (setq-default step-index (1- step-index)))
      ;; Remove the step from step-ring.
      (ring-remove step-ring step-number))))

(defun unlog-step-at-location ()
  "Removes any step from the step-ring that matches the current location.
It does nothing if no matching step is found."
  (interactive)
  (let* (
         ;; Get the current location.
         (location (footsteps--make-step))
         ;; Get the number of steps in the step-ring.
         (num-steps (ring-length step-ring))
         ;; Initialize step-number to num-steps (initial loop value).
         (step-number num-steps)
         ;; Initialize step-removed to nil. It will be set to t if a step gets removed.
         (step-removed nil)
         ;; Ensure the variable below is local.
         (step)
         )
    ;; Loop through the step-ring, and remove any steps that match the current location.
    (while (> step-number 0)
      (setq step-number (1- step-number))
      (setq step (footsteps--get-step step-number))
      (when (footsteps--step-equal-p location step)
        (progn
          (footsteps--unlog-step step-number)
          (setq step-removed t))))
    (if step-removed
        (message "The step has been unlogged.")
      (message "No step found at current location."))))

(defun footsteps--make-step ()
  "Returns a step at the current location."
  (interactive)
  (let* (
         ;; Give frm and win a default value of nil, which will be stored if footsteps--same-window-mode is non-nil.
         (frm nil)
         (win nil)
         )
    ;; Return a step for the current location.
    (unless footsteps--same-window-mode
      (progn
        (setq frm (selected-frame))
        (setq win (selected-window))))
    (list frm win (current-buffer) (min (footsteps--buffer-num-lines) (line-number-at-pos)))))

(defun footsteps--buffer-num-lines (&optional buffer)
  "Returns the number of lines in the current buffer."
  (interactive)
  (let* (
         (cbfr (current-buffer))
         (buffer (or buffer cbfr))
         (num-lines nil)
         )
    (with-current-buffer buffer (setq num-lines (count-lines (point-min) (point-max))))
    num-lines))

(defun footsteps--update-buffer-length-records ()
  "Loop through footsteps--buffer-length-ring, and update
the recorded length values to the current buffer lengths."
  (interactive)
  (let* (
         ;; Set record-number (initial loop value) to the number of records.
         (num-records (ring-length footsteps--buffer-length-ring))
         (record-number num-records)
         ;; Give current-buffer-found a default value of nil.
         (current-buffer-found nil)
         ;; Ensure the variables below are local.
         (record) (record-buffer)
         )
    ;; Loop through the records...
    (while (> record-number 0)
      (setq record-number (1- record-number))
      ;; Get the current record, and its corresponding buffer.
      (setq record (ring-ref footsteps--buffer-length-ring record-number))
      (setq record-buffer (nth 0 record))
      ;; If we've found the current-buffer in footsteps--buffer-length-ring, set current-buffer-found to t.
      (when (equal record-buffer (current-buffer)) (setq current-buffer-found t))
      ;; As long as the record buffer hasn't been killed...
      (if (buffer-live-p record-buffer)
          (progn
            ;; ...and as long as the record buffer isn't narrowed...
            (unless (with-current-buffer record-buffer (buffer-narrowed-p))
              (progn
                (setq record (list record-buffer (footsteps--buffer-num-lines record-buffer)))
                (footsteps--ring-set-element footsteps--buffer-length-ring record-number record))))
        ;; If the record-buffer has been killed, remove it from footsteps--buffer-length-ring.
        (ring-remove footsteps--buffer-length-ring record-number)))
    ;; If the current-buffer is not in footsteps--buffer-length-ring, then add it...
    (unless current-buffer-found
      (progn
        (setq record (list (current-buffer) (footsteps--buffer-num-lines)))
        (ring-insert+extend footsteps--buffer-length-ring record t)))))

(defun footsteps--adjust-steps ()
  (interactive)
  (let* (
         ;; Set record-number (initial loop value) equal to the number of records in footsteps--buffer-length-ring.
         (num-records (ring-length footsteps--buffer-length-ring))
         (record-number num-records)
         ;; Ensure the variables below are local.
         (record) (record-buffer) (current-line) (record-num-lines) (current-num-lines) (size-change) (num-steps)
         (step-number) (step) (step-frame) (step-window) (step-buffer) (step-line) (new-line) (new-step)
         )
    ;; Loop through the buffer length records...
    (while (> record-number 0)
      (setq record-number (1- record-number))
      ;; Get the record, and its corresponding buffer and previously-stored number of lines.
      (setq record (ring-ref footsteps--buffer-length-ring record-number))
      (setq record-buffer (nth 0 record))
      (setq record-num-lines (nth 1 record))
      ;; If the buffer is still live, and isn't narrowed...
      (when (and (buffer-live-p record-buffer) (not (with-current-buffer record-buffer (buffer-narrowed-p))))
        (progn
          ;; Get the line number at point, as well as an up-to-date number of lines for the record buffer.
          ;;   Then, compute the size change since the last record was logged.
          (setq current-line (with-current-buffer record-buffer (line-number-at-pos)))
          (setq current-num-lines (footsteps--buffer-num-lines record-buffer))
          (setq size-change (- current-num-lines record-num-lines))
          ;; If the buffer has changed sizes...
          ;;   Note that this conditional isn't strictly necessary for function, but it does
          ;;   decrease resource usage, as the loops nested in it will often be prevented from running.
          (unless (equal size-change 0)
            (progn
              ;; Loop through the steps...
              (setq num-steps (ring-length step-ring))
              (setq step-number num-steps)
              (while (> step-number 0)
                (setq step-number (1- step-number))
                ;; Get the step, as well as its corresponding frame, window, buffer and line.
                (setq step (footsteps--get-step step-number))
                (setq step-frame (nth 0 step))
                (setq step-window (nth 1 step))
                (setq step-buffer (nth 2 step))
                (setq step-line (nth 3 step))
                ;; If the step is in the record-buffer.
                (when (equal step-buffer record-buffer)
                  (progn
                    (cond
                     ;; If footsteps--delete-step-with-line is t, and the line the step was on was deleted, delete the step...
                     ((and footsteps--delete-step-with-line (< size-change 0) (>= step-line current-line) (< step-line (- current-line size-change)))
                      (progn
                        (footsteps--unlog-step step-number)))
                     ;; If the buffer shrank and the step was below or on the deleted region...
                     ((and (< size-change 0) (>= step-line (- current-line size-change)))
                      (progn
                        (setq new-line (max 1 (min current-num-lines (+ step-line size-change))))
                        (setq new-step (list step-frame step-window step-buffer new-line))
                        (footsteps--ring-set-element step-ring step-number new-step)))
                     ;; If the buffer grew and the step was below the added region...
                     ((and (> size-change 0) (> step-line (- current-line size-change)))
                      (progn
                        (setq new-line (max 1 (min current-num-lines (+ step-line size-change))))
                        (setq new-step (list step-frame step-window step-buffer new-line))
                        (footsteps--ring-set-element step-ring step-number new-step))))))))))))
    ;; Update footsteps--buffer-length-records, to prevent applying the same size change multiple times.
    (footsteps--update-buffer-length-records)))

(defun footsteps--buffer-open-p (buffername)
  "Predicate that returns t if the buffer with buffername is live.
Otherwise, it returns nil."
  (interactive)
  ;; Get a current list of live buffers.
  (setq bufferlist (mapcar (function buffer-name) (buffer-list)))
  ;; The default output value is set to nil.
  (setq buffer-open nil)
  ;; Loop through the list of buffers, setting the output value to t, if buffername is found.
  (dolist (buffer bufferlist)
    (when (string-equal buffer buffername) (setq buffer-open t)))
  buffer-open)

(defun footsteps--clean-step-ring ()
  "Performs cleanup actions on the step-ring.
This includes the removal of any steps which...
    ...are no longer accessible.
    ...would create a step-cycle shorter than footsteps--shortest-permissible-step-cycle.
This command runs itself in a loop, until one full pass is made over the step-ring, where nothing is removed."
  (interactive)
  (let* (
         ;; Initialize step-number (initial loop value) to the value of num-steps.
         (num-steps (ring-length step-ring))
         (step-number num-steps)
         ;; Initialize loop to t. This variable is used in the outermost while loop, to force a rerun when changes are made to the step-ring. It will be nil after the first clean pass.
         (loop t)
         ;; Ensure the variables below are local.
         (step) (frm) (win) (bfr) (offset) (second-step-number)
         )
    ;; Run this code until we have 1 full pass over the step-ring where nothing is unlogged.
    (while loop
      (setq loop nil)
      ;; Loop through steps, removing any step whose location can no longer be accessed.
      (setq step-number num-steps)
      (while (> step-number 0)
        (setq step-number (1- step-number))
        (setq step (footsteps--get-step step-number))
        (setq frm (nth 0 step))
        (setq win (nth 1 step))
        (setq bfr (nth 2 step)) 
        (unless (and
                 (or footsteps--same-window-mode (frame-live-p frm))
                 (or footsteps--same-window-mode (window-live-p win))
                 (footsteps--buffer-open-p (buffer-name bfr)))
          (progn
            (footsteps--unlog-step step-number)
            (setq loop t))))
      ;; If a step cycle is found that is shorter than footsteps--shortest-permissible-step-cycle, unlog the earlier of the 2 steps.
      (setq num-steps (ring-length step-ring))
      (setq step-number num-steps)
      (while (> step-number 1)
        (setq step-number (1- step-number))
        (setq offset 1)
        (while (< offset footsteps--shortest-permissible-step-cycle)
          (setq second-step-number (- step-number offset))
          (setq offset (1+ offset))
          (when (>= second-step-number 0)
            (when (footsteps--step-equal-p (footsteps--get-step step-number) (footsteps--get-step second-step-number))
              (progn
                (footsteps--unlog-step second-step-number)
                (setq loop t)))))))))

(defun footsteps--ring-set-element (ring pos elt)
  "Overwrites an existing element at pos in ring, with elt."
  "Note that this function was derived from ring-remove, defined in ring.el."
  (let* ((hd (car ring))
         (ln (cadr ring))
         (vec (cddr ring))
         (veclen (length vec)))
    (setq pos (ring-index pos hd ln veclen))
    (aset vec pos elt)
    (setcar (cdr ring) ln)))

(defun footsteps--goto-step (step-number)
  "Takes the user to the location defined by the step at step-number, in the step-ring."
  (interactive)
  (let* (
         ;; Get the step from step-number.
         (step (footsteps--get-step step-number))
         ;; Ensure the variable below is local.
         (step-buffer)
         )
    ;; As long as the step isn't nil (ie, out-of-bounds)...
    (when step
      ;; If either the to/from buffer are narrowed, do nothing, leaving a message for the user.
      (setq step-buffer (nth 2 step))
      (if (or (buffer-narrowed-p) (with-current-buffer step-buffer (buffer-narrowed-p)))
          (message "Please disable narrowing to continue.")
        ;; As long neither the to nor from buffers are narrowed...
        ;; Go to the appropriate frame and window (only if footsteps--same-window-mode-is-nil).
        (unless footsteps--same-window-mode
          (progn
            (select-frame (nth 0 step))
            (select-window (nth 1 step))))
        ;; Go to the appropriate buffer.
        (switch-to-buffer step-buffer)
        ;; Go to the appropriate line.
        (goto-line (nth 3 step))
        ;; Center the line, if possible.
        (recenter)
        ;; Update the step-index.
        (setq-default step-index step-number)))))

(defun footsteps--step-equal-p (step1 step2)
  "Returns t if step1 and step2 are considered equal, 
based in part on user-configurable settings."
  (interactive)
  (let* (
         ;; Default output value is nil.
         (res nil)
         )
    ;; Check whether or not the 2 steps are equal, setting the output value to t if they are.
    (when (and (equal (nth 0 step1) (nth 0 step2))
               (equal (nth 1 step1) (nth 1 step2))
               (equal (nth 2 step1) (nth 2 step2))
               (< (abs (- (nth 3 step1) (nth 3 step2))) footsteps--min-lines-before-step))
      (setq res t))
    res))

(defun footsteps--maintain-step-ring ()
  "Performs or initiates maintenance actions on the step-ring, footsteps--buffer-length-ring, and other global variables used by footsteps.
This command is called directly by post-command-hook, when footsteps-mode is enabled.
One of its functions is to selectively log the current location.
In the event that it doesn't log to prevent the creation of a cycle shorter than footsteps--shortest-permissible-step-cycle, 
it will set the step-index to the conflicting step."
  (interactive)
  (let* (
         ;; The default behavior is to log a step.
         (log-step t)
         ;; Get the current location.
         (location (footsteps--make-step))
         ;; Get the number of steps in the step-ring.
         (num-steps (ring-length step-ring))
         ;; Initialize step-number (initial loop value) to loop through the last "footsteps--shortest-permissible-step-cycle - 1" steps in the step-ring.
         (step-number (max 0 (1+ (- num-steps footsteps--shortest-permissible-step-cycle))))
         ;; Ensure variable below is local.
         (earlier-step)
         )
    ;; Create or fix the value of any global footsteps variable that needs it.
    (footsteps--correct-values)
    ;; Always adjust steps immediately, to prevent *size-change -> move to different spot -> then adjust* error (applies adjustment to wrong set of steps).
    (footsteps--adjust-steps)
    ;; Check whether or not the triggering command is in footsteps--abort-hook-commands.
    ;;   If it is, then set log-step to nil.
    (when (member this-command footsteps--abort-hook-commands)
      (setq log-step nil))
    ;; Check whether or not the current buffer is in footsteps--abort-hook-buffers.
    ;;   If it is, then set log-step to nil.
    (when (member (buffer-name) footsteps--abort-hook-buffers)
      (setq log-step nil))
    ;; Check to make sure this step won't create a cycle that is too short.
    ;;   If it does, then set log-step to nil.
    ;;     Also, set the current step-index to the conflicting step (ie, adjust our position in the ring, but do not modify the ring).
    (while (and log-step (< step-number num-steps))
      (setq earlier-step (footsteps--get-step step-number))
      (when (footsteps--step-equal-p earlier-step location)
        (progn
          (setq log-step nil)
          (setq-default step-index step-number)))
      (setq step-number (1+ step-number)))
    ;; If the current step-ring is empty, override everything above, and set log-step to t.
    (when (ring-empty-p step-ring) (setq log-step t))
    ;; If the location is in a minibuffer, override everything above, and set log-step to nil.
    (when (minibufferp) (setq log-step nil))
    ;; Check whether or not the current buffer is narrowed.
    ;;   If it is, then set log-step to nil.
    (when (buffer-narrowed-p) (setq log-step nil))
    ;; If log-step is t, log the step.
    (when log-step (footsteps--log-step))
    ;; Clean the step-ring every time hook is ran (since adjustment ran at the start of this function).
    (footsteps--clean-step-ring)))

(defun footsteps--log-step ()
  "Logs the current location as a step in the step-ring."
  (interactive)
  ;; If the step index is not at the most recent step, and the current location doesn't match the
  ;;   step at step-index, then branching has occurred.
  (unless (footsteps--step-equal-p (footsteps--make-step) (footsteps--get-step step-index))
    (progn
      ;; If branching has occurred, prune the step-ring back to the index.
      (while (> (1- (ring-length step-ring)) step-index)
        (footsteps--unlog-step -1))))
  ;; Put current step into step-ring.
  (ring-insert-at-beginning step-ring (footsteps--make-step))
  ;; Update the step-index.
  (setq-default step-index (1- (ring-length step-ring))))

(defun step-forward ()
  "Navigates forward through the step-ring."
  (interactive)
  ;; Create or fix the value of any global footsteps variable that needs it.
  (footsteps--correct-values)
  ;; Adjust and clean the step-ring prior to step-ring navigation.
  (footsteps--adjust-steps)
  (footsteps--clean-step-ring)
  ;; Go to the next step in the step-ring, if one exists.
  (footsteps--goto-step (1+ step-index)))

(defun step-backward ()
  "Navigates backward through the step-ring."
  (interactive)
  ;; Create or fix the value of any global footsteps variable that needs it.
  (footsteps--correct-values)
  ;; Adjust and clean the step-ring prior to step-ring navigation.
  (footsteps--adjust-steps)
  (footsteps--clean-step-ring)
  ;; If the step at step-index matches the current location, go to the previous step (or step 0 if that's the current step-index).
  ;;   Otherwise, go to the step at step-index.
  (if (footsteps--step-equal-p (footsteps--get-step step-index) (footsteps--make-step))
      (footsteps--goto-step (max 0 (1- step-index)))
    (footsteps--goto-step step-index)))

(provide 'footsteps)

;;; footsteps.el ends here
